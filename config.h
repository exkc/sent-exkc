/* See LICENSE file for copyright and license details. */

static char *fontfallbacks[] = {
"FiraCode Nerd Font"
"Chiron Sans HK Light"
};
#define NUMFONTSCALES 42
#define FONTSZ(x) ((int)(10.0 * powf(1.1288, (x)))) /* x in [0, NUMFONTSCALES-1] */

static const char *colors_one[] = {
	"#f8f8f2", /* foreground color */
	"#282a36", /* background color */
};

static const char *colors_two[] = {
	"#ff5555", /* foreground color */
	"#282a36", /* background color */
};

static const char *colors_three[] = {
	"#50fa7b", /* foreground color */
	"#282a36", /* background color */
};

static const char *colors_four[] = {
	"#f1fa8c", /* foreground color */
	"#282a36", /* background color */
};

static const char *colors_five[] = {
	"#ff79c6", /* foreground color */
	"#282a36", /* background color */
};

static const char *colors_six[] = {
	"#8be9fd", /* foreground color */
	"#282a36", /* background color */
};

static const char *colors_seven[] = {
	"#8fbcbb", /* foreground color */
	"#282a36", /* background color */
};

static const char *colors_eight[] = {
	"#88c0d0", /* foreground color */
	"#282a36", /* background color */
};

static const char *colors_nine[] = {
	"#81a1c1", /* foreground color */
	"#282a36", /* background color */
};

static const char *colors_ten[] = {
	"#5e81ac", /* foreground color */
	"#282a36", /* background color */
};


static const float linespacing = 1.4;

/* how much screen estate is to be used at max for the content */
static const float usablewidth = 0.75;
static const float usableheight = 0.75;

/* height of the presentation progress bar */
int topbar = 1;
int progressheight = 5;

static Mousekey mshortcuts[] = {
	/* button         function        argument */
	{ Button1,        advance,        {.i = +1} },
	{ Button3,        advance,        {.i = -1} },
	{ Button4,        advance,        {.i = -1} },
	{ Button5,        advance,        {.i = +1} },
};

static Shortcut shortcuts[] = {
	/* keysym         function        argument */
 {               XK_f,   fullscreen,      {0}},
	{ XK_Escape,      quit,           {0} },
	{ XK_q,           quit,           {0} },
	{ XK_Right,       advance,        {.i = +1} },
	{ XK_Left,        advance,        {.i = -1} },
	{ XK_Return,      advance,        {.i = +1} },
	{ XK_space,       advance,        {.i = +1} },
	{ XK_BackSpace,   advance,        {.i = -1} },
	{ XK_l,           advance,        {.i = +1} },
	{ XK_h,           advance,        {.i = -1} },
	{ XK_j,           advance,        {.i = +1} },
	{ XK_k,           advance,        {.i = -1} },
	{ XK_Down,        advance,        {.i = +1} },
	{ XK_Up,          advance,        {.i = -1} },
	{ XK_Next,        advance,        {.i = +1} },
	{ XK_Prior,       advance,        {.i = -1} },
	{ XK_n,           advance,        {.i = +1} },
	{ XK_p,           advance,        {.i = -1} },
	{ XK_r,           reload,         {0} },

	{ XK_1,           togglescm,  {.i = 1} },
	{ XK_2,           togglescm,  {.i = 2} },
	{ XK_3,           togglescm,  {.i = 3} },
	{ XK_4,           togglescm,  {.i = 4} },
	{ XK_5,           togglescm,  {.i = 5} },
	{ XK_6,           togglescm,  {.i = 6} },
	{ XK_7,           togglescm,  {.i = 7} },
	{ XK_8,           togglescm,  {.i = 8} },
	{ XK_9,           togglescm,  {.i = 9} },
	{ XK_0,           togglescm,  {.i = 10} },
};

static Filter filters[] = {
	{ "\\.ff$", "cat" },
	{ "\\.ff.bz2$", "bunzip2" },
	{ "\\.[a-z0-9]+$", "2ff" },
};
